#!/bin/bash

[ ! -d "./superpom" ] && cd ..
[ ! -d "./superpom" ] && cd ..

mvn clean -f integration-tests
ant clean -f deployment/maven
mvn clean -f okapi-ui/swt/core-ui
mvn clean
