package net.sf.okapi.common.resource;

import net.sf.okapi.common.filterwriter.GenericContent;
import net.sf.okapi.common.resource.TextFragment.TagType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static net.sf.okapi.common.resource.TextFragmentUtil.alignAndCopyCodeMetadata;
import static net.sf.okapi.common.resource.TextFragmentUtil.synchronizeCodeIds;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class TextFragmentUtilTest {
	private final GenericContent fmt = new GenericContent();

	/**
	 * Makes a fragment <code>[b]A[br/]B[/b]C<code>
	 * @return the new fragment.
	 */
	private TextFragment makeFragment1 () {
		TextFragment tf = new TextFragment();
		tf.append(TagType.OPENING, "b", "[b]");
		tf.append("A");
		tf.append(TagType.PLACEHOLDER, "br", "[br/]");
		tf.append("B");
		tf.append(TagType.CLOSING, "b", "[/b]");
		tf.append("C");
		return tf;
	}

	/**
	 * Makes a fragment <code>{B}A{/B}B{BR/}C extra<code>
	 * @return the new fragment.
	 */
	private TextFragment makeFragment1Bis (String extra) {
		TextFragment tf = new TextFragment();
		tf.append(TagType.OPENING, "b", "{B}");
		tf.append("A");
		tf.append(TagType.CLOSING, "b", "{/B}");
		tf.append("B");
		tf.append(TagType.PLACEHOLDER, "br", "{BR/}");
		tf.append("C ").append(extra);
		return tf;
	}

	private ITextUnit createTextUnit1 () {
		ITextUnit tu = new TextUnit("1", "t ");
		TextFragment tf = tu.getSource().getSegments().getFirstContent();
		tf.append(TagType.OPENING, "b", "<b>");
		tf.append("bold");
		tf.append(TagType.CLOSING, "b", "</b>");
		tf.append(" t ");
		tf.append(TagType.PLACEHOLDER, "br", "<br/>");
		return tu;
	}

	@Test
	public void missingCodesWithoutDeletable() {
		TextFragment src = new TextFragment();
		TextFragment trg = new TextFragment();
		src.append(new Code(TagType.PLACEHOLDER, "c", "c"));
		src.append(new Code(TagType.PLACEHOLDER, "c1", "c1"));
		src.append(new Code(TagType.PLACEHOLDER, "c2", "c2"));
		
		trg.append(new Code(TagType.PLACEHOLDER, "c", "c"));
		CodeMatches m = synchronizeCodeIds(src, trg);
		assertNotNull(m);
		assertTrue(m.hasFromMismatch());
		assertFalse(m.hasToMismatch());
	}
	
	@Test
	public void missingCodesWithDuplicates() {
		TextFragment src = new TextFragment();
		TextFragment trg = new TextFragment();
		src.append(new Code(TagType.PLACEHOLDER, "c", "c"));
		src.append(new Code(TagType.PLACEHOLDER, "c1", "c1"));
		src.append(new Code(TagType.PLACEHOLDER, "c2", "c2"));
		src.append(new Code(TagType.CLOSING, "c2", "c2"));
		
		trg.append(new Code(TagType.PLACEHOLDER, "c", "c"));

		CodeMatches m = synchronizeCodeIds(src, trg);
		assertNotNull(m);
		assertTrue(m.hasFromMismatch());
		assertFalse(m.hasToMismatch());
	}
	
	@Test
	public void missingCodesWithDeletable() {
		TextFragment src = new TextFragment();
		TextFragment trg = new TextFragment();
		src.append(new Code(TagType.PLACEHOLDER, "c", "c"));
		src.append(new Code(TagType.PLACEHOLDER, "c1", "c1"));		
		src.append(new Code(TagType.PLACEHOLDER, "c2", "c2"));
		
		trg.append(new Code(TagType.PLACEHOLDER, "c", "c"));
		Code dc = new Code(TagType.PLACEHOLDER, "c2", "c2");
		dc.setDeleteable(true);
		trg.append(dc);

		CodeMatches m = synchronizeCodeIds(src, trg);
		assertNotNull(m);
		assertTrue(m.hasFromMismatch());
		assertFalse(m.hasToMismatch());
	}
	
	@Test
	public void addedCodesWithoutDeletable() {
		TextFragment src = new TextFragment();
		TextFragment trg = new TextFragment();
		src.append(new Code(TagType.PLACEHOLDER, "c", "c"));
		
		trg.append(new Code(TagType.PLACEHOLDER, "c1", "c1"));
		trg.append(new Code(TagType.PLACEHOLDER, "c2", "c2"));
		trg.append(new Code(TagType.PLACEHOLDER, "c", "c"));

		CodeMatches m = synchronizeCodeIds(src, trg);
		assertNotNull(m);
		assertFalse(m.hasFromMismatch());
		assertTrue(m.hasToMismatch());
	}
	
	@Test
	public void addedCodesWithDeletable() {
		TextFragment src = new TextFragment();
		TextFragment trg = new TextFragment();
		src.append(new Code(TagType.PLACEHOLDER, "c", "c"));
		src.append(new Code(TagType.PLACEHOLDER, "c1", "c1"));
		
		trg.append(new Code(TagType.PLACEHOLDER, "c2", "c2"));		
		Code dc = new Code(TagType.PLACEHOLDER, "c3", "c3");
		dc.setDeleteable(true);
		trg.append(dc);

		CodeMatches m = synchronizeCodeIds(src, trg);
		assertNotNull(m);
		assertFalse(m.hasFromMismatch());
		assertFalse(m.hasToMismatch());
	}
	
	@Test
	public void noMissingOrAddedCodes() {
		TextFragment src = new TextFragment("test");
		TextFragment trg = new TextFragment("test");

		CodeMatches m = synchronizeCodeIds(src, trg);
		assertFalse(m.hasFromMismatch());
		assertFalse(m.hasToMismatch());
	}


	@Test
	public void testAdjustTargetFragment () {
		TextFragment toTransSrc = makeFragment1();
		TextFragment proposalTrg = makeFragment1Bis("trg");
		assertEquals("{B}A{/B}B{BR/}C trg", proposalTrg.toText());
		alignAndCopyCodeMetadata(toTransSrc, proposalTrg, true, true);
		assertEquals("[b]A[/b]B[br/]C trg", proposalTrg.toText());
	}

	@Test
	public void testAdjustIncompleteTargetFragmentAutoAdded () {
		TextFragment toTransSrc = makeFragment1();
		TextFragment proposalTrg = makeFragment1Bis("trg");
		proposalTrg.remove(6, 8); // "xxAxxBxxC trg"
		assertEquals("{B}A{/B}BC trg", proposalTrg.toText());
		alignAndCopyCodeMetadata(toTransSrc, proposalTrg,  true, true);
		assertEquals("[b]A[/b]BC trg[br/]", proposalTrg.toText());
	}

	@Test
	public void testAdjustIncompleteTargetFragmentNoAddition () {
		TextFragment toTransSrc = makeFragment1();
		TextFragment proposalTrg = makeFragment1Bis("with warning");
		proposalTrg.remove(6, 8); // "xxAxxBxxC with warning"
		assertEquals("{B}A{/B}BC with warning", proposalTrg.toText());
		alignAndCopyCodeMetadata(toTransSrc, proposalTrg, false, true);
		assertEquals("[b]A[/b]BC with warning", proposalTrg.toText());
	}

	@Test
	public void testAdjustNoCodes () {
		ITextUnit tu = new TextUnit("1", "src");
		TextFragment newTrg = new TextFragment("trg");
		alignAndCopyCodeMetadata(tu.getSource().getSegments().getFirstContent(), newTrg, false, true);
		assertEquals("trg", newTrg.toText());
	}

	@Test
	public void testAdjustSameMarkers () {
		ITextUnit tu = createTextUnit1();
		TextFragment tf = new TextFragment("T ");
		tf.append(TagType.OPENING, "b", "<T>");
		tf.append("BOLD");
		tf.append(TagType.CLOSING, "b", "</T>");
		tf.append(" T ");
		tf.append(TagType.PLACEHOLDER, "br", "<PH/>");
		alignAndCopyCodeMetadata(tu.getSource().getSegments().getFirstContent(), tf, false, true);
		assertEquals("T <b>BOLD</b> T <br/>", tf.toText());
		fmt.setContent(tf);
		assertEquals("T <1>BOLD</1> T <2/>", fmt.toString());
	}

	@Test
	public void testAdjustExtraMarkers () {
		ITextUnit tu = createTextUnit1();
		TextFragment tf = new TextFragment("T ");
		tf.append(TagType.OPENING, "b", "<T>");
		tf.append("BOLD");
		tf.append(TagType.CLOSING, "b", "</T>");
		tf.append(" T ");
		tf.append(TagType.PLACEHOLDER, "br", "<PH/>");
		tf.append(TagType.PLACEHOLDER, "extra", "<EXTRA/>");
		alignAndCopyCodeMetadata(tu.getSource().getSegments().getFirstContent(), tf, true, true);
		assertEquals("T <b>BOLD</b> T <br/><EXTRA/>", tf.toText());
		fmt.setContent(tf);
		assertEquals("T <1>BOLD</1> T <2/><3/>", fmt.toString());
	}

	@Test
	public void testAdjustMissingMarker () {
		ITextUnit tu = createTextUnit1();
		TextFragment tf = new TextFragment("T ");
		tf.append(TagType.OPENING, "b", "<T>");
		tf.append("BOLD");
		tf.append(" T ");
		tf.append(TagType.PLACEHOLDER, "br", "<PH/>");
		tf.append(TagType.PLACEHOLDER, "extra", "<EXTRA/>");
		tf.balanceMarkers();
		alignAndCopyCodeMetadata(tu.getSource().getSegments().getFirstContent(), tf, false, true);
		assertEquals("T <b>BOLD T <br/><EXTRA/>", tf.toText());
		fmt.setContent(tf);
		assertEquals("T <b1/>BOLD T <2/><3/>", fmt.toString());
	}

	@Test
	public void testAdjustDifferentTextSameMarkers () {
		ITextUnit tu = createTextUnit1();
		TextFragment tf = new TextFragment("U ");
		tf.append(TagType.OPENING, "b", "<b>");
		tf.append("BOLD");
		tf.append(TagType.CLOSING, "b", "</b>");
		tf.append(" U ");
		tf.append(TagType.PLACEHOLDER, "br", "<br/>");
		// Fuzzy match but codes are the same
		alignAndCopyCodeMetadata(tu.getSource().getFirstContent(), tf, false, true);
		assertEquals("U <b>BOLD</b> U <br/>", tf.toText());
		assertEquals("U <1>BOLD</1> U <2/>", fmt.setContent(tf).toString());
	}

	@Test
	public void testMovedCodes () {
		TextFragment oriFrag = new TextFragment("s1 ");
		oriFrag.append(TagType.PLACEHOLDER, "c1", "[c1]");
		oriFrag.append(" s2 ");
		oriFrag.append(TagType.OPENING, "c2", "[c2>]");
		oriFrag.append(" s3 ");
		oriFrag.append(TagType.CLOSING, "c2", "[<c2]");
		TextFragment trgFrag = GenericContent.fromLetterCodedToFragment("<g2>t3</g2> t1 <x1/> t2", null, false, true);
		alignAndCopyCodeMetadata(oriFrag, trgFrag, false, true);
		assertEquals("[c2>]t3[<c2] t1 [c1] t2", fmt.setContent(trgFrag).toString(true));
	}

	@Test
	public void testCodesWithSameId () {
		TextFragment oriFrag = new TextFragment("s1 ");
		oriFrag.append(TagType.PLACEHOLDER, "c1", "[c1]");
		oriFrag.append(" s2 ");
		oriFrag.append(TagType.PLACEHOLDER, "c1", "[c1]");
		oriFrag.append(" s3 ");
		TextFragment trgFrag = GenericContent.fromLetterCodedToFragment("<x1/>t3<x1/> t1 t2", null, false, true);
		alignAndCopyCodeMetadata(oriFrag, trgFrag, false, true);
		assertEquals("[c1]t3[c1] t1 t2", fmt.setContent(trgFrag).toString(true));
	}

	@Test
	public void testAddMissingCodes() {
		TextFragment oriFrag;
		TextFragment trgFrag;

		oriFrag = GenericContent.fromLetterCodedToFragment("src<x1/><x2/>", null, false, true);
		trgFrag = GenericContent.fromLetterCodedToFragment("trg<x2/>", null, false, true);
		alignAndCopyCodeMetadata(oriFrag, trgFrag, true, true);
		assertEquals("trg<2/><1/>", fmt.setContent(trgFrag).toString());

		oriFrag = GenericContent.fromLetterCodedToFragment("src<x1/>", null, false, true);
		trgFrag = GenericContent.fromLetterCodedToFragment("trg", null, false, true);
		alignAndCopyCodeMetadata(oriFrag, trgFrag, true, true);
		assertEquals("trg<1/>", fmt.setContent(trgFrag).toString());

		oriFrag = GenericContent.fromLetterCodedToFragment("src<x1/>", null, false, true);
		trgFrag = GenericContent.fromLetterCodedToFragment("trg<x2/>", null, false, true);
		alignAndCopyCodeMetadata(oriFrag, trgFrag, true, true);
		assertEquals("trg<1/>", fmt.setContent(trgFrag).toString());

		oriFrag = GenericContent.fromLetterCodedToFragment("<x1/>src<x2/>", null, false, true);
		trgFrag = GenericContent.fromLetterCodedToFragment("trg<x2/>", null, false, true);
		alignAndCopyCodeMetadata(oriFrag, trgFrag, true, true);
		assertEquals("<1/>trg<2/>", fmt.setContent(trgFrag).toString());

		oriFrag = GenericContent.fromLetterCodedToFragment("<x1/> src", null, false, true);
		trgFrag = GenericContent.fromLetterCodedToFragment("trg", null, false, true);
		alignAndCopyCodeMetadata(oriFrag, trgFrag, true, true);
		assertEquals("<1/>trg", fmt.setContent(trgFrag).toString());

		oriFrag = GenericContent.fromLetterCodedToFragment("<x1/> src<x2/>", null, false, true);
		trgFrag = GenericContent.fromLetterCodedToFragment("trg", null, false, true);
		alignAndCopyCodeMetadata(oriFrag, trgFrag, true, true);
		assertEquals("<1/>trg<2/>", fmt.setContent(trgFrag).toString());

		oriFrag = GenericContent.fromLetterCodedToFragment("<x1/> src<x2/>", null, false, true);
		trgFrag = GenericContent.fromLetterCodedToFragment("<x2/>trg", null, false, true);
		alignAndCopyCodeMetadata(oriFrag, trgFrag, true, true);
		assertEquals("<1/><2/>trg", fmt.setContent(trgFrag).toString());
	}

	@Test
	public void testTransferWithMissingClose() {
		// source: <i>Before</i> <b>bold<i> after.
		// target: <b>XXXXXX</b> <i>XXXX</i> XXXX.
		TextFragment tf = new TextFragment("s1");
		tf.append(TagType.OPENING, "i", "<i>", 1);
		tf.append("s2");
		tf.append(TagType.CLOSING, "i", "</i>", 1);
		tf.append(" ");
		tf.append(TagType.OPENING, "b", "<b>");
		tf.append("s3");
		tf.append(TagType.OPENING, "i", "<i>");
		tf.append("s3");
		assertEquals("s1<i>s2</i> <b>s3<i>s3", tf.toText());
		assertEquals("s1<1>s2</1> <b2/>s3<b3/>s3", fmt.setContent(tf).toString());
		TextContainer srcOri = new TextContainer(tf);

		tf = new TextFragment("s1");
		tf.append(TagType.OPENING, "b", "<b>", 1);
		tf.append("s2");
		tf.append(TagType.CLOSING, "b", "</b>", 1);
		tf.append(" ");
		tf.append(TagType.OPENING, "i", "<i>");
		tf.append("s3");
		tf.append(TagType.CLOSING, "i", "</i>");
		tf.append("s3");
		assertEquals("s1<b>s2</b> <i>s3</i>s3", tf.toText());
		assertEquals("s1<1>s2</1> <2>s3</2>s3", fmt.setContent(tf).toString());
		TextContainer trgTra = new TextContainer(tf);

		alignAndCopyCodeMetadata(srcOri.getFirstContent(), trgTra.getFirstContent(), true, true);

		assertEquals("s1<i>s2</i> <i>s3</i>s3<i>", trgTra.getFirstContent().toText());
		assertEquals("s1<1>s2</1> <2>s3</2>s3<b3/>", fmt.setContent(trgTra.getFirstContent()).toString());
	}

	@Test
	public void testTransferWithMissingCloseSimple() {
		// source: <i>Before</i><i>
		// target: <b>XXXXXX</b>
		TextFragment tf = new TextFragment("");
		tf.append(TagType.OPENING, "i", "<i>", 1);
		tf.append("s2");
		tf.append(TagType.CLOSING, "i", "</i>", 1);
		tf.append(TagType.OPENING, "i", "<i>");
		assertEquals("<i>s2</i><i>", tf.toText());
		assertEquals("<1>s2</1><b2/>", fmt.setContent(tf).toString());
		TextContainer srcOri = new TextContainer(tf);

		tf = new TextFragment("");
		tf.append(TagType.OPENING, "b", "<b>", 1);
		tf.append("s2");
		tf.append(TagType.CLOSING, "b", "</b>", 1);

		assertEquals("<b>s2</b>", tf.toText());
		assertEquals("<1>s2</1>", fmt.setContent(tf).toString());
		TextContainer trgTra = new TextContainer(tf);

		alignAndCopyCodeMetadata(srcOri.getFirstContent(), trgTra.getFirstContent(), true, true);

		assertEquals("<i>s2</i><i>", trgTra.getFirstContent().toText());
		assertEquals("<1>s2</1><b2/>", fmt.setContent(trgTra.getFirstContent()).toString());
	}

	@Test
	public void missingCodesWithZeroInTarget() {
		TextFragment oriFrag = GenericContent.fromLetterCodedToFragment("xxx <g1>yyy</g1> zzz", null, false, true);
		TextFragment trgFrag = GenericContent.fromLetterCodedToFragment("xxx yyy zzz", null, false, true);
		alignAndCopyCodeMetadata(oriFrag, trgFrag, true, false);
		assertEquals("xxx yyy zzz<1></1>", fmt.setContent(trgFrag).toString());
	}

	@Test
	public void transferMissingGroupVsStandalone() {
		//<link>X.
		//<ph x="1"/>X.
		TextFragment tf1 = new TextFragment();
		Code c = new Code(TagType.OPENING, "link", "<link>");
		tf1.append(c);
		tf1.append("X.");
		// mark <link> as an isolated code
		tf1.balanceMarkers();
		assertEquals("<link>X.", tf1.toText());
		// Make sure the coded text has the proper marker.
		assertTrue(tf1.getCodedText().contains("\uE103"));


		TextFragment tf2 = new TextFragment();
		c = new Code(TagType.PLACEHOLDER, "", "");
		c.setOuterData("<ph x=\"1\"/>");
		tf2.append(c);
		tf2.append("X.");
		tf2.balanceMarkers();
		assertEquals("X.", tf2.toText());
		// Make sure the coded text has the proper marker.
		assertTrue(tf2.getCodedText().contains("\uE103"));

		alignAndCopyCodeMetadata(tf1, tf2, true, true);
		assertEquals("<link>X.", tf2.toText());
		assertEquals(1, tf2.getCodes().size());
		assertEquals(TagType.OPENING, tf2.getCodes().get(0).getTagType());
	}

	@Test
	public void testAlignAndCopyCodeMetadataWhenSrcHasCodeFromHasNone() {
		// testing alignAndCopyCodeMetadata with "src" that has a OPENING code at the end and "from" that doesn't have any.
		TextFragment srcTf = new TextFragment();
		Code c = new Code(TagType.OPENING, "footnote", "<footnote>");
		srcTf.append("abc");
		srcTf.append(c);

		// mark <link> as an isolated code
		srcTf.balanceMarkers();
		assertEquals("abc<footnote>", srcTf.toText());
		// Make sure the coded text has the proper marker.
		assertNotEquals("The Isolated marker is expected", -1, srcTf.getCodedText().indexOf(TextFragment.MARKER_ISOLATED));
		TextFragment fromTf = new TextFragment();
		fromTf.append("ABC");

		alignAndCopyCodeMetadata(srcTf, fromTf, true, true);
		assertEquals(1, fromTf.getCodes().size());
		c = fromTf.getCode(0);
		assertEquals(TagType.OPENING, c.tagType);
		assertEquals("<footnote>", c.getData());
		assertEquals("ABC<footnote>", fromTf.toText());
		assertNotEquals("The Isolated marker is expected", -1, fromTf.getCodedText().indexOf(TextFragment.MARKER_ISOLATED));	}
}
