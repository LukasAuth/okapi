/*===========================================================================
  Copyright (C) 2021 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.applications.tikal;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import org.slf4j.event.Level;

import net.sf.okapi.applications.IOkapiLoggerAppender;
import net.sf.okapi.common.UserConfiguration;

public class TikalLoggerAppender implements IOkapiLoggerAppender {
	private static Level level = Level.INFO;
	private static final String consoleEncodingName = getConsoleEncodingName();
	private static final PrintStream ps = getEncodedPrintStream(consoleEncodingName);

	@Override
	public void log(String loggerName, Level level, String msg, Throwable throwable) {
		ps.println(msg);
		if (throwable != null && level.toInt() <= Level.TRACE.toInt()) {
			throwable.printStackTrace(ps);
		}
	}

	@Override
	public Level getLevel() {
		return level;
	}

	@Override
	public void setLevel(Level newLevel) {
		level = newLevel;
	}

	/**
	 * Try the guess the encoding of the console.
	 *
	 * System.out uses the default system encoding that may not be the right one
	 * (e.g. windows-1252 vs cp850)
	 *
	 * @return the guessed name of the console's encoding.
	 */
	private static String getConsoleEncodingName() {
		String osName = System.getProperty("os.name");
		String enc = null;
		if (osName.contains("OS X")) {
			enc = "UTF-8"; // Apparently the default for bash on Mac
		} else if (osName.startsWith("Windows")) {
			enc = "cp850"; // Not perfect, but covers many languages
		} else {
			// Default: Assumes unique encoding overall
			enc = Charset.defaultCharset().name();
		}
		// Now check if we have a user setting
		UserConfiguration uc = new UserConfiguration();
		uc.load("Tikal");
		return uc.getProperty("displayEncoding", enc);
	}

	private static PrintStream getEncodedPrintStream(String encoding) {
		try {
			return new PrintStream(System.out, true, encoding);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace(System.out);
			return System.out;
		}
	}

	public String getEncodingName() {
		return consoleEncodingName;
	}
}
